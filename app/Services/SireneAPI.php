<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use SolidairesInformatique\AccessToken;
use SolidairesInformatique\AccessTokenException;
use SolidairesInformatique\BadParameter;
use SolidairesInformatique\CurlException;
use SolidairesInformatique\Etablissement;
use SolidairesInformatique\Sirene;

class SireneAPI
{
    private const SIRENE_CACHE_KEY = 'sirene-access-token';
    private const SIRENE_CACHE_ETABLISSEMENT = 'sirene-tablissement-';
    private static ?Sirene $sirene = null;

    /**
     * @throws CurlException
     * @throws BadParameter
     * @throws AccessTokenException
     */
    public function getWorkplace(string $siret): Etablissement
    {
        $etablissement = Cache::get(self::SIRENE_CACHE_ETABLISSEMENT.$siret);
        if ($etablissement instanceof Etablissement) {
            return $etablissement;
        }

        $etablissement = $this->getSirene()->siret($siret);
        Cache::set(self::SIRENE_CACHE_ETABLISSEMENT.$siret, $etablissement, 86400);

        return $etablissement;
    }

    /**
     * @throws CurlException
     * @throws BadParameter
     * @throws AccessTokenException
     */
    public function getHeadOffice(Etablissement $workplace): Etablissement
    {
        if ($workplace->etablissementSiege) {
            return $workplace;
        }

        return $this->getWorkplace($workplace->getHeadOfficeSiret());
    }

    /**
     * @throws CurlException
     * @throws AccessTokenException
     */
    private function getSirene(): Sirene
    {
        if (self::$sirene instanceof Sirene) {
            return self::$sirene;
        }

        $accessToken = Cache::get(self::SIRENE_CACHE_KEY);
        if (!$accessToken instanceof AccessToken) {
            $accessToken = AccessToken::get(config('adhesion.sirene.key'), config('adhesion.sirene.secret'));
            Cache::set(self::SIRENE_CACHE_KEY, $accessToken, $accessToken->expiresIn - 600);
        }

        self::$sirene = new Sirene($accessToken);

        return self::$sirene;
    }
}
