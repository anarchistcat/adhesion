<?php

declare(strict_types=1);

namespace App\Services;

use App\Enums\OptionEnum;
use App\Enums\Role;
use App\Mail\ApplicationReceivedConfirmation;
use App\Mail\WelcomeNotification;
use App\Models\Application;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

readonly class NotificationService
{
    public function sendApplicationReceivedConfirmation(Application $application): void
    {
        Mail::to($application->email)->send(new ApplicationReceivedConfirmation($application));
    }

    public function sendWelcomeNotification(Application $application): void
    {
        Mail::to($application->email)->send(new WelcomeNotification($application->refresh()));
    }

    /**
     * @return string[]
     */
    public function getEmailsOfUsersWithOption(OptionEnum $option, mixed $value, Role $minimumRole = Role::INACTIVE): array
    {
        return User::query()
            ->select('users.email')
            ->join('options', 'options.user_id', '=', 'users.id')
            ->where('users.role', '<=', $minimumRole)
            ->where('options.key', $option->value)
            ->where('options.value', $value)
            ->whereNotNull('users.email')
            ->pluck('users.email')
            ->toArray();
    }
}
