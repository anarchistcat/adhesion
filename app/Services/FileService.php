<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Application;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Mpdf\Output\Destination;

readonly class FileService
{
    public function __construct(
        private Mpdf $mpdf,
    ) {
    }

    /**
     * @throws MpdfException
     *                       TODO: Async
     */
    public function saveApplicationAsPDF(Application $application): void
    {
        try {
            $this->mpdf->WriteHTML(view('application.raw', ['application' => $application, 'carbonFactory' => new \Carbon\Factory([
                'locale' => 'fr_FR',
                'timezone' => 'Europe/Paris',
            ])])->render());
        } catch (MpdfException $e) {
            Log::error($e->getMessage());
        }

        $pdfTitle = "{$application->firstname} {$application->name} {$application->getShortenedId()}.pdf";

        $fs = Storage::disk('nextcloud');
        if ($fs->exists('') === false) {
            throw new \RuntimeException("Couldn't access the directory on nextcloud");
        }
        $fs->put($pdfTitle, $this->mpdf->Output($pdfTitle, Destination::STRING_RETURN));
    }
}
