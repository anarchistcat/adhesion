<?php

declare(strict_types=1);

namespace App\Livewire;

trait HasTextFields
{
    /**
     * @param array<int, string> $rules
     *
     * @return array<int, string>
     */
    public static function getTextValidators(bool $required, array $rules = []): array
    {
        return [$required ? 'required' : 'nullable', 'string', 'max:255', ...$rules];
    }
}
