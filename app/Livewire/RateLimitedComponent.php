<?php

declare(strict_types=1);

namespace App\Livewire;

use Illuminate\Support\Facades\RateLimiter;
use Livewire\Component;

abstract class RateLimitedComponent extends Component
{
    final protected function triedLessThanAMinuteAgo(string $rateLimiterKey): bool
    {
        if (!auth()->guest()) {
            return false;
        }

        if (RateLimiter::tooManyAttempts($rateLimiterKey, 1)) {
            $seconds = RateLimiter::availableIn($rateLimiterKey);
            $this->addError('submit', __('validation.throttled_with_seconds', ['seconds' => $seconds]));

            return true;
        }

        RateLimiter::hit($rateLimiterKey);

        return false;
    }
}
