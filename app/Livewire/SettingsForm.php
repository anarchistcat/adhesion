<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Models\Option;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class SettingsForm extends Component
{
    use HasTextFields;

    /** @var Option[] */
    public array $options;

    public function mount(): void
    {
    }

    public function submit(): void
    {
        $this->validate();

        $success = true;
        foreach ($this->options as $option) {
            if ($option->isClean()) {
                continue;
            }
            $success = $success && $option->update();
            Log::info("Option updated : {$option}.");
        }

        if ($success) {
            session()->flash('success_message', 'Paramètres mis à jour.');
        } else {
            session()->flash('error_message', "Quelque chose s'est mal passé.");
        }

        $this->redirect(route('admin.settings'));
    }

    /**
     * @return array<string, string[]>
     */
    public function rules(): array
    {
        return [];
    }
}
