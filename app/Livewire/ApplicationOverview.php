<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Models\Application;
use Livewire\Component;

class ApplicationOverview extends Component
{
    public Application $application;
    public bool $showRestrictedInformation;
}
