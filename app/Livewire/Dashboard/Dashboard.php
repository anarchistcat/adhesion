<?php

declare(strict_types=1);

namespace App\Livewire\Dashboard;

use Livewire\Attributes\Title;
use Livewire\Attributes\Url;
use Livewire\Component;

#[Title("Gestion des demands d'adhésions")]
class Dashboard extends Component
{
    #[Url]
    public string $filter = '';
}
