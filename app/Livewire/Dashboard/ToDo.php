<?php

declare(strict_types=1);

namespace App\Livewire\Dashboard;

use App\Enums\ApplicationStatus;
use App\Models\Application;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Attributes\On;
use Livewire\Attributes\Url;
use Livewire\Component;

class ToDo extends Component
{
    /** @var ApplicationStatus[] */
    private const array STATUSES = [
        ApplicationStatus::NEW,
        ApplicationStatus::VALIDATED,
        ApplicationStatus::COMPLETED,
        ApplicationStatus::READY,
    ];

    /** @var array<string, int> */
    public array $counts;

    #[Url]
    public string $filter = '';

    /** @var string[] */
    public array $notifications = [];

    private User $user;

    public function __construct()
    {
        $user = Auth::user();
        if (!$user instanceof User) {
            throw new \RuntimeException("Couldn't find user.");
        }
        $this->user = $user;
    }

    public function mount(): void
    {
        foreach (self::STATUSES as $status) {
            $notification = $status->notificationOption();
            if ($notification === null || $this->user->option($notification)->value !== '1') {
                continue;
            }
            $this->notifications[] = $status->value;
        }

        $this->updateCounts();
    }

    public function updatedNotifications(): void
    {
        foreach (self::STATUSES as $status) {
            $notification = $status->notificationOption();
            if ($notification === null) {
                Log::warning("Can't update notification for status {$status->name}.");

                continue;
            }
            $this->user->option($notification)->setBool(
                \in_array($status->value, $this->notifications, true)
            );
        }
    }

    #[On('move-application')]
    public function updateCounts(): void
    {
        foreach (self::STATUSES as $status) {
            $this->counts[$status->value] = Application::query()->where('status', $status)->count();
        }
    }
}
