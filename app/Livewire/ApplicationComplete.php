<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Enums\ApplicationStatus;
use App\Models\Application;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class ApplicationComplete extends Component
{
    use HasTextFields;

    public Application $application;

    /**
     * @throws AuthorizationException
     */
    public function complete(): void
    {
        $this->validate();

        $this->authorize('complete', Application::class);
        $this->application->status = ApplicationStatus::COMPLETED;
        $this->application->update();
        Log::notice("Application {$this->application->id} completed. RUM: {$this->application->RUM}");

        redirect(route('dashboard.index'));
    }

    /**
     * @return array<string, array<string>>
     */
    public function rules(): array
    {
        return [
            'application.RUM' => self::getTextValidators(false, [ApplicationForm::REQUIRED_IF_SEPA]),
            'application.first_withdrawal_date' => [ApplicationForm::REQUIRED_IF_SEPA, 'nullable', 'date', 'after_or_equal:tomorrow'],
        ];
    }
}
