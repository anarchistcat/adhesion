<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Enums\ContributionWay;
use App\Models\Application;
use App\Rules\Iban;
use App\Services\SireneAPI;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\Enum;
use Livewire\Attributes\On;
use SolidairesInformatique\AccessTokenException;
use SolidairesInformatique\BadParameter;
use SolidairesInformatique\CurlException;
use Spatie\Honeypot\Http\Livewire\Concerns\HoneypotData;
use Spatie\Honeypot\Http\Livewire\Concerns\UsesSpamProtection;

class ApplicationForm extends RateLimitedComponent
{
    use ConvertEmptyStringsToNull;
    use HasTextFields;
    use UsesSpamProtection;

    public Application $application;
    public bool $employee = true;
    public bool $minimalContribution = false;
    public string $wage = '';
    public float $contributionPercentage = 0.5;
    public bool $terms = false;
    public int $step = 1;
    public bool $noSiret = false;
    private SireneAPI $sireneAPI;
    public HoneypotData $extraFields;

    private const SIRET_FIELD = 'application.company_siret';
    public const REQUIRED_IF_SEPA = 'required_if:application.contribution_way,'.ContributionWay::SEPA->value;

    public function boot(SireneAPI $sireneAPI): void
    {
        $this->sireneAPI = $sireneAPI;
    }

    public function mount(): void
    {
        $this->extraFields = new HoneypotData();

        if (request()->query->has('SIRET')) {
            $this->application->company_siret = (string) request()->query->get('SIRET');
            $this->updated(self::SIRET_FIELD);
        }
    }

    /**
     * @return array<string, mixed>
     */
    protected function rules(): array
    {
        return \call_user_func_array('array_merge', $this->rulesByStep());
    }

    public function updated(string $propertyName): void
    {
        $this->validateOnly($propertyName);

        if ($propertyName === 'wage' || $propertyName === 'minimalContribution') {
            $this->updateContribution();
        }

        if ($this->noSiret === false && $propertyName === self::SIRET_FIELD) {
            $this->updateCompanyWithSiret();
        }
    }

    public function next(): void
    {
        $this->validate($this->rulesByStep()[$this->step]);

        if ($this->step === 2) {
            $this->updateSepaPrefill();
        }

        ++$this->step;
    }

    public function back(): void
    {
        --$this->step;
    }

    /**
     * @throws \Exception
     */
    public function submit(): void
    {
        $this->protectAgainstSpam();
        $this->validate();

        if ($this->triedLessThanAMinuteAgo('send-application:'.request()->getClientIp())) {
            return;
        }

        if (!$this->application->save()) {
            $this->addError('submit', __('validation.server_error'));
            Log::error('validation.server_error');

            return;
        }

        Log::info("Application {$this->application->id} submitted.");
        $this->redirect(route('success'));
    }

    #[On('updateSignature')]
    public function setSignature(string $signature_base64 = ''): void
    {
        $this->application->signature = $signature_base64;
        $this->updated('application.signature');
    }

    public function incrementContribution(): void
    {
        $this->application->contribution = floor($this->application->contribution) + 1;
    }

    /**
     * @return array<int<1, 4>, array<string, array<mixed>>>
     */
    private function rulesByStep(): array
    {
        return
            [
                1 => [
                    'application.name' => self::getTextValidators(true),
                    'application.firstname' => self::getTextValidators(true),
                    'application.address' => self::getTextValidators(true),
                    'application.postcode' => ['required', 'numeric', 'integer'],
                    'application.city' => self::getTextValidators(true),
                    'application.email' => self::getTextValidators(true, ['email']),
                    'application.mobile' => self::getTextValidators(true),
                    'application.birth_year' => ['nullable', 'numeric', 'min:1900', 'max:'.date('Y')],
                    'application.gender' => self::getTextValidators(false),
                ],
                2 => [
                    'application.company_name' => self::getTextValidators(false),
                    self::SIRET_FIELD => ['nullable', 'digits:14'],
                    'application.company_legal_address' => self::getTextValidators(false),
                    'application.company_work_address' => self::getTextValidators(false),
                ],
                3 => [
                    'wage' => ['nullable', 'numeric', 'min:0'],
                    'application.contribution' => ['required', 'numeric', 'min:1'],
                    'application.contribution_way' => ['required', new Enum(ContributionWay::class)],
                    'application.sepa_full_name' => self::getTextValidators(false, [self::REQUIRED_IF_SEPA]),
                    'application.sepa_address' => self::getTextValidators(false, [self::REQUIRED_IF_SEPA]),
                    'application.sepa_postcode' => [self::REQUIRED_IF_SEPA, 'nullable', 'numeric', 'integer'],
                    'application.sepa_city' => self::getTextValidators(false, [self::REQUIRED_IF_SEPA]),
                    'application.sepa_country' => [self::REQUIRED_IF_SEPA, 'nullable', 'in:France'],
                    'application.iban' => [self::REQUIRED_IF_SEPA, 'nullable', new Iban()],
                    'application.bic' => self::getTextValidators(false, [self::REQUIRED_IF_SEPA, 'nullable', 'alpha_num']),
                    'application.made_at' => self::getTextValidators(false, [self::REQUIRED_IF_SEPA]),
                    'application.signature' => [self::REQUIRED_IF_SEPA, 'nullable', 'nullable', 'min:64'],
                ],
                4 => [
                    'application.mail_local' => ['nullable', 'boolean'],
                    'application.applicant_comment' => ['nullable', 'max:500'],

                    'terms' => ['accepted'],
                ],
            ];
    }

    private function updateContribution(): void
    {
        if ($this->minimalContribution) {
            $this->contributionPercentage = 0;
            $this->application->contribution = 1;

            return;
        }

        $this->contributionPercentage = $this->wage <= 1600 ? 0.5 : 0.65;
        $this->application->contribution = max(round((float) $this->wage * ($this->contributionPercentage / 100), 2), 1);
    }

    private function updateCompanyWithSiret(): void
    {
        try {
            $workplace = $this->sireneAPI->getWorkplace((string) $this->application->company_siret);
        } catch (AccessTokenException $exception) {
            Log::warning($exception->getMessage());
            $this->addErrorToSiretField();

            return;
        } catch (BadParameter $exception) {
            Log::debug($exception->getMessage());
            $this->addErrorToSiretField($exception->getMessage());

            return;
        } catch (CurlException $exception) {
            Log::notice($exception->getMessage());
            $this->addErrorToSiretField($exception->getCode() === 404 ? (string) __('form.siret.404') : null);

            return;
        }

        $this->application->company_name = $workplace->uniteLegale->denominationUniteLegale;
        $this->application->company_work_address = $workplace->adresse->__toString();

        try {
            $headOffice = $this->sireneAPI->getHeadOffice($workplace);
            $this->application->company_legal_address = $headOffice->adresse->__toString();
        } catch (\Throwable $exception) {
            Log::notice($exception->getMessage());
        }
    }

    private function addErrorToSiretField(?string $message = null): void
    {
        $this->validateOnly(
            self::SIRET_FIELD,
            [self::SIRET_FIELD => fn (string $attr, mixed $val, \Closure $fail) => $fail($message ?? __('form.siret.generic'))]
        );
    }

    private function updateSepaPrefill(): void
    {
        $this->application->sepa_full_name = $this->application->name.' '.$this->application->firstname;
        $this->application->sepa_address = $this->application->address;
        $this->application->sepa_postcode = $this->application->postcode;
        $this->application->sepa_city = $this->application->city;
        $this->application->sepa_country = 'France';
    }
}
