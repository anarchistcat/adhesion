<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Enums\ApplicationStatus;
use App\Models\Application;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class ShowApplication extends Component
{
    use HasTextFields;

    public Application $application;
    public bool $showDone;
    public string $doneName;
    public bool $showReject;

    private User $user;

    public function __construct()
    {
        $user = Auth::user();
        if (!$user instanceof User) {
            throw new \RuntimeException("Couldn't find user.");
        }
        $this->user = $user;
    }

    public function mount(): void
    {
        $this->showDone = $this->showDoneAction();
        $this->doneName = match ($this->application->status) {
            ApplicationStatus::NEW => __('dashboard.actions.validate'),
            ApplicationStatus::VALIDATED => __('dashboard.actions.complete'),
            ApplicationStatus::COMPLETED, ApplicationStatus::READY => __('dashboard.actions.done'),
            default => '',
        };
        $this->showReject = $this->showRejectAction();
    }

    public function reject(): void
    {
        $this->application->status = ApplicationStatus::REJECTED;
        $this->application->update();

        Log::notice("Application {$this->application->id} rejected.");
        $this->dispatch('move-application', applicationId: $this->application->id);
    }

    public function done(): void
    {
        if ($this->application->status === ApplicationStatus::VALIDATED) {
            $this->redirect(route('dashboard.application.complete', [$this->application]));

            return;
        }

        $this->application->status = $this->application->status->next();
        $this->application->update();

        Log::notice("Application {$this->application->id} {$this->application->status->value}.");
        $this->dispatch('move-application', applicationId: $this->application->id);
    }

    private function showDoneAction(): bool
    {
        if (\in_array($this->application->status, [ApplicationStatus::REJECTED, ApplicationStatus::WELCOMED], true)) {
            return false;
        }

        if ($this->application->status === ApplicationStatus::VALIDATED) {
            return $this->user->can('complete', Application::class);
        }

        return true;
    }

    private function showRejectAction(): bool
    {
        if (!\in_array($this->application->status, [ApplicationStatus::NEW, ApplicationStatus::VALIDATED], true)) {
            return false;
        }

        return $this->user->can('reject', Application::class);
    }
}
