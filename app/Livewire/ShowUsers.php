<?php

declare(strict_types=1);

namespace App\Livewire;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class ShowUsers extends Component
{
    /**
     * @var Collection<int, User>
     */
    public Collection $users;

    public function mount(): void
    {
        $this->users = User::all();
    }
}
