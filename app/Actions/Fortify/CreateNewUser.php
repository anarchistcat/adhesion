<?php

declare(strict_types=1);

namespace App\Actions\Fortify;

use App\Enums\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Enum;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    public const RULES = [
        'pseudo' => ['required', 'string', 'max:255'],
        'email' => ['nullable', 'string', 'email', 'max:255'],
    ];

    /**
     * Create a newly registered user.
     *
     * @param array<string, mixed> $input
     *
     * @throws ValidationException
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'pseudo' => [...self::RULES['pseudo'], 'unique:users'],
            'email' => [...self::RULES['email'], 'unique:users'],
            'role' => ['nullable', new Enum(Role::class)],
            'password' => $this->passwordRules(),
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
        ])->validate();

        return DB::transaction(function () use ($input) {
            return tap(User::create([
                'pseudo' => $input['pseudo'],
                'email' => $input['email'] ?? null,
                'role' => $input['role'] ?? Role::INACTIVE,
                'password' => Hash::make($input['password']),
            ]), function (User $user): void {
            });
        });
    }
}
