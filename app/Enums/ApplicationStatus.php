<?php

declare(strict_types=1);

namespace App\Enums;

enum ApplicationStatus: string
{
    case NEW = 'new';
    case VALIDATED = 'validated';
    case COMPLETED = 'completed';
    case READY = 'ready';
    case WELCOMED = 'welcomed';
    case REJECTED = 'rejected';

    /**
     * @see \App\Policies\ApplicationPolicy
     */
    public function ability(): string
    {
        return match ($this) {
            self::NEW => 'create',
            self::VALIDATED => 'validate',
            self::COMPLETED => 'complete',
            self::READY => 'ready',
            self::REJECTED => 'reject',
            self::WELCOMED => 'welcome',
        };
    }

    public function notificationOption(): ?OptionEnum
    {
        return match ($this) {
            self::NEW => OptionEnum::NEW_RECAP,
            self::VALIDATED => OptionEnum::VALIDATED_RECAP,
            self::COMPLETED => OptionEnum::COMPLETED_RECAP,
            self::READY => OptionEnum::READY_RECAP,
            self::REJECTED, self::WELCOMED => null,
        };
    }

    /**
     * @throws \Exception
     */
    public function next(): self
    {
        return match ($this) {
            self::NEW => self::VALIDATED,
            self::VALIDATED => self::COMPLETED,
            self::COMPLETED => self::READY,
            self::READY => self::WELCOMED,
            self::WELCOMED => throw new \Exception('There is no step after '.self::WELCOMED->value),
            self::REJECTED => throw new \Exception('There is no step after '.self::REJECTED->value),
        };
    }
}
