<?php

declare(strict_types=1);

namespace App\Enums;

enum OptionEnum: string
{
    case NEW_RECAP = 'new_recap';
    case VALIDATED_RECAP = 'validated_recap';
    case COMPLETED_RECAP = 'completed_recap';
    case READY_RECAP = 'ready_recap';
}
