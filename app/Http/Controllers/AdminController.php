<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Jackiedo\LogReader\LogReader;

class AdminController extends Controller
{
    public const ROUTE_NAMES = [
        'users' => [
            'index' => 'admin.users',
            'create' => 'admin.users.create',
            'edit' => 'admin.users.edit',
        ],
    ];

    public function viewLogs(LogReader $reader): Factory|View|Application
    {
        $logs = $reader->orderBy('date', 'desc')->get();

        return view('admin.logs', ['logs' => $logs]);
    }

    public function clearLogs(LogReader $reader): Redirector|Application|RedirectResponse
    {
        $reader->markAsRead();
        Log::notice('Logs cleared.');

        return redirect(route('admin.logs'));
    }

    public function viewUsers(): Factory|View|Application
    {
        return view('admin.users.index');
    }
}
