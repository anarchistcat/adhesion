<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EnsureUserHasSufficientRole
{
    public function handle(Request $request, \Closure $next, int $role): \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
    {
        $user = $request->user();
        if ($user instanceof User && $user->role->value <= $role) {
            return $next($request);
        }

        abort(Response::HTTP_FORBIDDEN);
    }
}
