<?php

declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\ValidationRule;

class Iban implements ValidationRule
{
    public function validate(string $attribute, mixed $value, \Closure $fail): void
    {
        $withoutSpaces = str_replace(' ', '', $value ?? '');

        // Move the four initial characters to the end of the string
        $rearranged = substr($withoutSpaces, 4).substr($withoutSpaces, 0, 4);

        // Replace each letter in the string with two digits, thereby expanding the string, where A = 10, B = 11, ..., Z = 35
        $convertedToIntegers = preg_replace_callback('/[A-Z]/', [$this, 'letterMatchedToDigit'], $rearranged);

        if ($convertedToIntegers === null) {
            $fail('validation.iban')->translate();

            return;
        }

        // Piece-wise calculation D mod 97
        $integersGroups = str_split($convertedToIntegers, 9);
        $remainder = '';
        foreach ($integersGroups as $integers) {
            $remainder = (string) ((int) ((int) $remainder.(int) $integers) % 97);
        }

        // If the remainder is 1, the check digit test is passed and the IBAN might be valid.
        if ($remainder !== '1') {
            $fail('validation.iban')->translate();
        }
    }

    /**
     * @param array<int|string, string> $matches
     */
    private function letterMatchedToDigit(array $matches): int
    {
        return \ord($matches[0]) - 55;
    }
}
