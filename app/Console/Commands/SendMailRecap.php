<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Enums\ApplicationStatus;
use App\Enums\Role;
use App\Mail\BoardToDoRecap;
use App\Services\NotificationService;
use App\Services\ToDoService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendMailRecap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-mail-recap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail notifications of awaiting tasks';

    /**
     * Execute the console command.
     */
    public function handle(ToDoService $toDoService, NotificationService $notificationService): void
    {
        $todo = $toDoService->getApplicantNames();

        $notified = [];
        foreach ($todo as $status => $applications) {
            $notification = ApplicationStatus::from($status)->notificationOption();
            if ($notification === null) {
                continue;
            }
            $notified = [
                ...$notified,
                ...$notificationService->getEmailsOfUsersWithOption(
                    $notification,
                    '1',
                    Role::BOARD,
                ),
            ];
        }
        $notified = array_unique($notified);

        Mail::bcc($notified)->send(new BoardToDoRecap($todo));

        try {
            Log::info('Recap sent to: '.json_encode($notified, \JSON_THROW_ON_ERROR));
        } catch (\JsonException $e) {
            Log::error($e->getMessage(), $e->getTrace());
        }
    }
}
