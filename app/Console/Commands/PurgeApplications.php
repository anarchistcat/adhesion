<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Enums\ApplicationStatus;
use App\Models\Application;
use Illuminate\Console\Command;

class PurgeApplications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'applications:purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop old applications from the database.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $count = Application::whereIn('status', [
            ApplicationStatus::REJECTED,
            ApplicationStatus::COMPLETED,
        ])->where('updated_at', '<', date('Y-m-d', strtotime('-1 month', time())))->delete();

        $this->info("{$count} deleted applications");

        return Command::SUCCESS;
    }
}
