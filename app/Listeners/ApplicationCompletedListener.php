<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\ApplicationCompletedByTreasury;
use App\Services\FileService;

readonly class ApplicationCompletedListener
{
    public function __construct(
        private FileService $fileService,
    ) {
    }

    public function handle(ApplicationCompletedByTreasury $event): void
    {
        $this->fileService->saveApplicationAsPDF($event->application);
    }
}
