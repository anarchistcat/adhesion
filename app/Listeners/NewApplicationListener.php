<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\NewApplication;
use App\Services\NotificationService;

readonly class NewApplicationListener
{
    public function __construct(
        private NotificationService $notificationService
    ) {
    }

    public function handle(NewApplication $event): void
    {
        $this->notificationService->sendApplicationReceivedConfirmation($event->application);
    }
}
