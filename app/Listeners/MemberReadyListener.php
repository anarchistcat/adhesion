<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\MemberReady;
use App\Services\NotificationService;

readonly class MemberReadyListener
{
    public function __construct(
        private NotificationService $notificationService,
    ) {
    }

    public function handle(MemberReady $event): void
    {
        $this->notificationService->sendWelcomeNotification($event->application);
    }
}
