<?php

declare(strict_types=1);

namespace App\Policies;

use App\Enums\Role;
use App\Models\User;

class ApplicationPolicy
{
    public function create(?User $user): bool
    {
        return true;
    }

    public function validate(User $user): bool
    {
        return $user->role->value <= Role::BOARD->value;
    }

    public function complete(User $user): bool
    {
        return $user->role === Role::TREASURY || $user->role === Role::ADMIN;
    }

    public function ready(User $user): bool
    {
        return $user->role->value <= Role::BOARD->value;
    }

    public function welcome(User $user): bool
    {
        return $user->role->value <= Role::BOARD->value;
    }

    public function reject(User $user): bool
    {
        return $user->role->value <= Role::BOARD->value;
    }
}
