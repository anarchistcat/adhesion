<?php

declare(strict_types=1);

namespace App\Policies;

use App\Enums\Role;
use App\Models\Option;
use App\Models\User;

class OptionPolicy
{
    public function update(User $user, Option $option): bool
    {
        return $user->role === Role::ADMIN || ($option->user_id !== null && $option->user_id === $user->id);
    }
}
