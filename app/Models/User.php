<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\OptionEnum;
use App\Enums\Role;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property int     $id
 * @property string  $pseudo
 * @property string  $password
 * @property ?string $remember_token
 * @property ?string $email
 * @property Role    $role
 * @property Carbon  $created_at
 * @property Carbon  $updated_at
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;

    protected $fillable = [
        'pseudo', 'email', 'password', 'role',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'role' => Role::class,
    ];

    protected $appends = [
        'profile_photo_url',
    ];

    /**
     * @return Collection<int, User>|array<User>
     */
    public static function getUsersWithRole(Role $role): Collection|array
    {
        return self::query()->where('role', $role)->get();
    }

    public function option(OptionEnum $key): Option
    {
        return Option::get($key, $this->id);
    }
}
