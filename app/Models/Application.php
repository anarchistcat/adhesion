<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\ApplicationStatus;
use App\Enums\ContributionWay;
use App\Events\ApplicationCompletedByTreasury;
use App\Events\MemberReady;
use App\Events\NewApplication;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;
use Ramsey\Uuid\Uuid;

/**
 * @property string            $id
 * @property string            $name
 * @property string            $firstname
 * @property string            $address
 * @property string            $postcode
 * @property string            $city
 * @property string            $email
 * @property string            $mobile
 * @property ?string           $company_name
 * @property ?string           $company_siret
 * @property ?string           $company_legal_address
 * @property ?string           $company_work_address
 * @property ?int              $birth_year
 * @property ?string           $gender
 * @property float             $contribution
 * @property ContributionWay   $contribution_way
 * @property bool              $mail_local
 * @property ApplicationStatus $status
 * @property Carbon            $created_at
 * @property Carbon            $updated_at
 * @property ?string           $applicant_comment
 * @property ?string           $sepa_full_name
 * @property ?string           $sepa_address
 * @property ?string           $sepa_postcode
 * @property ?string           $sepa_city
 * @property ?string           $sepa_country
 * @property ?string           $iban
 * @property ?string           $bic
 * @property ?string           $made_at
 * @property ?string           $signature
 * @property ?string           $RUM
 * @property ?string           $first_withdrawal_date
 */
class Application extends Model
{
    use HasFactory;
    use HasUuids;

    public $fillable = [
        'name',
        'firstname',
        'address',
        'postcode',
        'city',
        'email',
        'mobile',
        'company_name',
        'company_siret',
        'company_legal_address',
        'company_work_address',
        'birth_year',
        'gender',
        'contribution',
        'sepa_full_name',
        'sepa_address',
        'sepa_postcode',
        'sepa_city',
        'sepa_country',
        'iban',
        'bic',
        'made_at',
        'signature',
        'applicant_comment',
        'rum',
        'first_withdrawal_date',
    ];

    protected $casts = [
        'contribution_way' => ContributionWay::class,
        'status' => ApplicationStatus::class,
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array<string, mixed>
     */
    protected $attributes = [
        'status' => ApplicationStatus::NEW,
        'mail_local' => false,
        'contribution' => 1,
        'contribution_way' => ContributionWay::SEPA,
    ];

    public static function boot(): void
    {
        parent::boot();

        self::creating(static function (self $application): void {
            if ($application->contribution_way !== ContributionWay::SEPA) {
                $application->removeSepaData();
            }
        });

        self::saving(static function (self $application): void {
            Gate::authorize($application->status->ability(), $application);
        });

        self::saved(static function (self $application): void {
            match ($application->status) {
                ApplicationStatus::NEW => NewApplication::dispatch($application),
                ApplicationStatus::COMPLETED => ApplicationCompletedByTreasury::dispatch($application),
                ApplicationStatus::READY => MemberReady::dispatch($application),
                ApplicationStatus::WELCOMED, ApplicationStatus::REJECTED, ApplicationStatus::VALIDATED => null,
            };
        });
    }

    public function newUniqueId(): string
    {
        return (string) Uuid::uuid4();
    }

    public function getShortenedId(): string
    {
        return substr($this->id, 0, 8);
    }

    public function getTitle(): string
    {
        return sprintf('%s %s. (%s)', $this->firstname, $this->name[0], $this->getShortenedId());
    }

    public function removeSepaData(): void
    {
        $this->sepa_full_name = null;
        $this->sepa_address = null;
        $this->sepa_postcode = null;
        $this->sepa_city = null;
        $this->sepa_country = null;
        $this->iban = null;
        $this->bic = null;
        $this->made_at = null;
        $this->signature = null;
    }
}
