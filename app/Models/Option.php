<?php

declare(strict_types=1);

namespace App\Models;

use App\Enums\OptionEnum;
use App\Policies\OptionPolicy;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Cache;

/**
 * @property int     $id
 * @property string  $key
 * @property ?string $value
 * @property ?int    $user_id
 * @property Carbon  $updated_at
 */
class Option extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = ['key', 'value', 'user_id'];

    private function set(string $value): static
    {
        $this->value = $value;
        $this->save();

        return $this;
    }

    public function setBool(bool $value): static
    {
        return $this->set($value ? '1' : '0');
    }

    public static function boot(): void
    {
        parent::boot();

        static::updating(static function (self $model): void {
            $user = request()->user();
            /* @see OptionPolicy::update() */
            if ($user !== null && $user->cannot('update', $model)) {
                abort(403);
            }
            $model->updated_at = Carbon::now();
        });

        static::updated(static fn (self $model) => Cache::forget(self::cacheKey($model->key, $model->user_id)));
    }

    public static function get(OptionEnum $key, ?int $userId = null): self
    {
        return Cache::rememberForever(
            self::cacheKey($key->value, $userId),
            static fn () => self::query()->firstOrCreate(['key' => $key->value, 'user_id' => $userId])
        );
    }

    private static function cacheKey(string $key, ?int $userId): string
    {
        return "option_{$userId}_{$key}";
    }

    /**
     * @return HasOne<User>
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class);
    }
}
