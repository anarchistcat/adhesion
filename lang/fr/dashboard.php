<?php

use App\Enums\ApplicationStatus;

return [
    'application_requests' => "Demandes d'adhésion",
    'accepted' => "Adhésion :id marquée comme acceptée.",
    'rejected' => "Adhésion :id marquée comme refusée.",
    'no_applications_found' => "Aucune demande d'adhésion trouvée.",
    'status' => [
        \App\Enums\ApplicationStatus::NEW->value => 'Reçue :timeDiff',
        \App\Enums\ApplicationStatus::VALIDATED->value => 'Validée :timeDiff',
        \App\Enums\ApplicationStatus::COMPLETED->value => 'Complétée :timeDiff',
        \App\Enums\ApplicationStatus::READY->value => 'Finalisée :timeDiff',
        \App\Enums\ApplicationStatus::WELCOMED->value => 'Archivée :timeDiff',
        \App\Enums\ApplicationStatus::REJECTED->value => 'Rejetée :timeDiff',
    ],
    'role' => [
        \App\Enums\Role::INACTIVE->value => 'Désactivé·e',
        \App\Enums\Role::BOARD->value => 'Membre du bureau',
        \App\Enums\Role::ADMIN->value => 'Administrat·ice·eur',
        \App\Enums\Role::TREASURY->value => 'Trésorier·e',
    ],
    'users' => [
        'create' => 'Créer un·e utilisateur·ice',
    ],
    'todo' => [
        ApplicationStatus::NEW->value => 'Valider',
        ApplicationStatus::VALIDATED->value => 'Completer',
        ApplicationStatus::COMPLETED->value => 'Inscrire aux listes mails',
        ApplicationStatus::READY->value => 'Accueillir',
    ],
    'todo-description' => [
        ApplicationStatus::NEW->value => "Nouvelle demande d'adhésion",
        ApplicationStatus::VALIDATED->value => 'Nouvelle adhésion validée par le bureau',
        ApplicationStatus::COMPLETED->value => 'Nouvelle adhésion complétée par la trésorerie',
        ApplicationStatus::READY->value => 'Adhérent·e inscrit·e aux mailings lists',
        ApplicationStatus::WELCOMED->value => "Demande d'adhésion archivée.",
        ApplicationStatus::REJECTED->value => "Demande d'adhésion rejetée",
    ],
    'todo-instruction' => [
        ApplicationStatus::NEW->value  => 'elle doit être validée par le bureau (vérification du champs de syndicalisation, adhésion légitime…).',
        ApplicationStatus::VALIDATED->value  => 'elle doit être complétée par la trésorerie.',
        ApplicationStatus::COMPLETED->value  => "l'adhérent·e doit être inscrit·e aux listes mails pour recevoir son mail de bienvenue.",
        ApplicationStatus::READY->value  => "l'adhérent·e doit être accueillit.",
    ],
    'actions' => [
        'validate' => 'Valider',
        'complete' => 'Completer',
        'done' => 'Fait',
        'reject' => 'Refuser',
    ]
];
