<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Logs
        </h2>
    </x-slot>


    <div class="overflow-hidden bg-white shadow sm:rounded-lg">
        <div class="border-t border-gray-200">
            @php($i = 0)
            @foreach($logs as $key => $log)
                <div class="@if($i % 2) bg-gray-50 @endif px-4 py-5 sm:px-6">
                    <details class="mt-1">
                        <summary>
                            <span class="@if($log->level === 'error') text-red-500 @endif">[{{ $log->level }}]</span> {{ $log->getOriginal('date') }} -
                            {{ $log->context->message }}
                            at {{ $log->context->in }}:{{ $log->context->line }}
                        </summary>
                        <pre class="text-gray-900">{{ $log->getOriginal('stack_traces') }}</pre>
                    </details>
                </div>
                @php(++$i)
            @endforeach
        </div>

        <div class="mt-4">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="px-4 py-5 sm:px-6">
                    <a class="form__submit form__submit_error" href="{{ route('admin.logs.clear') }}">Marquer toutes les entrées comme lues</a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
