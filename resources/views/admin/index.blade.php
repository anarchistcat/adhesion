<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Administration
        </h2>
    </x-slot>

    <div class="mt-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <nav>
                    <ul>
{{--                        <li><a class="link" href="{{ route('admin.settings') }}">Paramètres</a></li>--}}
                        <li><a class="link" href="{{ route('admin.users') }}">Gestion des utilisateur·ice·s</a></li>
                        <li><a class="link" href="{{ route('admin.logs') }}">Voir les logs</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <div class="mt-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <h2 class="text-xl font-bold">Version :</h2>
                <p>
                    {{ file_get_contents($ref) }}
                </p>
                <h2 class="text-xl font-bold mt-4">Environment :</h2>
                <h3 class="font-bold">App:</h3>
                <ul>
                    @foreach(['env', 'debug', 'url', 'timezone', 'locale'] as $key)
                        <li>{{ $key }}: <code>{{config("app.{$key}")}}</code></li>
                    @endforeach
                </ul>

                <h3 class="font-bold mt-2">Cache:</h3>
                <p>Default cache driver is <code>{{config('cache.default')}}</code>:</p>
                <pre>{{ print_r(config('cache.stores.' . config('cache.default')), true) }}</pre>

                <h3 class="font-bold mt-2">Mail:</h3>
                <ul>
                    <li>default: <code>{{config('mail.default')}}</code></li>
                    <li>
                        {{config('mail.default')}}: <pre>{{ print_r(config('mail.mailers.' . config('mail.default')), true) }}</pre>
                    </li>
                    <li>from: <code>{{config('mail.from.name')}} &lt;{{config('mail.from.address')}}&gt;</code></li>
                </ul>
            </div>
        </div>
    </div>
</x-app-layout>
