<x-guest-layout>
    <div class="w-full max-w-6xl mx-auto px-6">
        <livewire:application-form :application="new \App\Models\Application()" />
    </div>
</x-guest-layout>
