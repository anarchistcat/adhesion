<x-guest-layout>
    <div class="py-12 w-full max-w-4xl mx-auto px-6">
        <h1 class="text-4xl">Nous avons bien reçu ta demande d'adhésion.</h1>

        <p>Nous la traiterons dans les meilleurs délais.</p>
        <p>Si ta situation nécessite une aide d'urgence, contacte nous par mail.</p>
    </div>
</x-guest-layout>
