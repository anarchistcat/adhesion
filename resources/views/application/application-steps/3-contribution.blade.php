<div class="form__section">
    <h2 class="form__section__instruction">
        Votre cotisation mensuelle dépend de votre rémunération nette. Elle permet de financer l'activité du syndicat.
    </h2>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label">
                <input wire:model.live="minimalContribution" name="employee" class="mr-2 leading-tight" type="checkbox">
                <span>Je demande la cotisation minimale de 1€ par mois</span>
            </label>
            <p>
                La cotisation minimale est prévu dans nos status, pour les adhérent∙es en grande difficulté financière.
            </p>
        </div>
    </div>

        @if(!$minimalContribution)
            <div class="form__input-group">
                <div class="form__input-group__element">
                    <label class="form__input-group__element__label" for="wage">
                        {{ __('validation.attributes.wage') }}
                    </label>
                    <div class="relative">
                        <input
                            wire:model.blur="wage"
                            name="wage"
                            id="wage"
                            type="number"
                            min=O
                            step=1
                            autocomplete="transaction-amount"
                            class="form__input-group__element__input"
                        >
                        <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-12">
                            <span class="text-gray-800 sm:text-sm">€</span>
                        </div>
                    </div>

                    <p class="form__input-group__element__instruction">
                        Nous n'enregistrons pas votre salaire, il sert uniquement à calculer votre taux de cotisation.
                    </p>

                    @error('wage') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
                </div>
            </div>
        @endif

    <div class="form__input-group">
        <div class="form__input-group__element form__input-group__element_1/2">
            <p class="form__input-group__element__label" >
                Taux de cotisation
            </p>
            <p class="form__input-group__element__p">{{ $this->contributionPercentage }} %</p>
        </div>

        <div class="form__input-group__element form__input-group__element_1/2">
            <p class="form__input-group__element__label" >
                {{ __('validation.attributes.contribution') }}
            </p>
            <p class="form__input-group__element__p flex justify-between">{{ $application->contribution }} <button wire:click.prevent="incrementContribution">+</button></p>
            <p class="form__input-group__element__instruction">
                Si vous le souhaitez, vous pouvez indiquer une cotisation plus importante (avec le bouton <button wire:click.prevent="incrementContribution">+</button>).
            </p>
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label" for="contribution_way">
                {{ __('validation.attributes.contribution_way') }}
            </label>
            <select
                wire:model.live="application.contribution_way"
                name="contribution_way"
                id="contribution_way"
                required
                class="form__input-group__element__input"
            >
                @foreach(\App\Enums\ContributionWay::cases() as $contributionWay)
                    <option value="{{ $contributionWay }}" >
                        {{ __("form.contribution_way_value.$contributionWay->value") }}
                    </option>
                @endforeach
            </select>
            <p class="form__input-group__element__instruction @if($application->contribution_way !== \App\Enums\ContributionWay::SEPA) text-red-800 font-bold @endif">
                Pour de multiples raisons logistiques, nous préférons les règlements par prélèvements automatiques.
            </p>
            @if($application->contribution_way === \App\Enums\ContributionWay::CHECK)
                <p class="form__input-group__element__instruction text-red-800 font-bold">
                    Si vous ne réglez pas votre cotisation par prélèvements automatiques, préférez les virements aux chèques.
                </p>
            @endif
            @error('application.contribution_way') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>
</div>

@if($application->contribution_way === \App\Enums\ContributionWay::SEPA)
    <div class="form__section">
    <h2 class="form__section__instruction">Mandat de prélèvement SEPA</h2>

    <p>
        En signant ce formulaire de mandat, vous autorisez le Créancier à envoyer des instructions à votre banque pour débiter votre compte, et votre banque à
        débiter votre compte conformément aux instructions du Créancier.
        Vous bénéficiez du droit d’être remboursé par votre banque selon les conditions décrites dans la convention que vous avez passé avec elle.
        Une demande de remboursement doit être présentée dans les 8 semaines suivant la date de débit de votre compte pour un prélèvement autorisé.
    </p>

    <div class="my-4">
        <h3 class="font-bold">Créancier :</h3>
        <p>
            Syndicat Solidaires Informatique, 31 rue de la Grange Aux Belles 75001 Paris France.
        <p>
            Identifiant Créancier SEPA (ICS) : FR 23 ZZZ 458309
        </p>
    </div>

    <h3 class="font-bold">Débiteur :</h3>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label" for="full_name">
                {{ __('validation.attributes.full_name') }}
            </label>
            <input
                wire:model.blur="application.sepa_full_name"
                id="full_name"
                name="full_name"
                type="text"
                autocomplete="name"
                required
                class="form__input-group__element__input @error('application.sepa_full_name') errored @enderror"
            >
            @error('application.sepa_full_name') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label" for="address">
                {{ __('validation.attributes.address') }}
            </label>
            <input
                wire:model.blur="application.sepa_address"
                name="address"
                type="text"
                id="address"
                required
                autocomplete="street-address"
                class="form__input-group__element__input @error('application.sepa_address') errored @enderror"
            >
            @error('application.sepa_address') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element form__input-group__element_1/2">
            <label class="form__input-group__element__label" for="postcode">
                {{ __('validation.attributes.postcode') }}
            </label>
            <input
                wire:model.blur="application.sepa_postcode"
                name="postcode"
                type="text"
                id="postcode"
                required
                autocomplete="postal-code"
                class="form__input-group__element__input @error('application.sepa_postcode') errored @enderror"
            >
            @error('application.sepa_postcode') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>

        <div class="form__input-group__element form__input-group__element_1/2">
            <label class="form__input-group__element__label" for="city">
                {{ __('validation.attributes.city') }}
            </label>
            <input
                wire:model.blur="application.sepa_city"
                name="city"
                type="text"
                id="city"
                required
                autocomplete="city"
                class="form__input-group__element__input @error('application.sepa_city') errored @enderror"
            >
            @error('application.sepa_city') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <p class="form__input-group__element__label" >
                {{ __('validation.attributes.country') }}
            </p>
            <p class="form__input-group__element__p">France</p>
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label" for="iban">
                {{ __('validation.attributes.iban') }}
            </label>
            <input
                wire:model.blur="application.iban"
                name="iban"
                type="text"
                id="iban"
                required
                class="form__input-group__element__input @error('application.iban') errored @enderror"
            >
            @error('application.iban') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element">
            <label class="form__input-group__element__label" for="bic">
                {{ __('validation.attributes.bic') }}
            </label>
            <input
                wire:model.blur="application.bic"
                name="bic"
                type="text"
                id="bic"
                required
                class="form__input-group__element__input @error('application.bic') errored @enderror"
            >
            @error('application.bic') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>
    </div>

    <div class="form__input-group">
        <div class="form__input-group__element form__input-group__element_1/2">
            <label class="form__input-group__element__label" for="made_at">
                {{ __('validation.attributes.made_at') }}
            </label>
            <input
                wire:model.blur="application.made_at"
                name="made_at"
                type="text"
                id="made_at"
                required
                autocomplete="city"
                class="form__input-group__element__input @error('application.made_at') errored @enderror"
            >
            @error('application.made_at') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
        </div>

        <div class="form__input-group__element form__input-group__element_1/2">
            <p class="form__input-group__element__label" >
                {{ __('form.current_date') }}
            </p>
            <p class="form__input-group__element__p">{{ now()->format('d/m/Y') }}</p>
        </div>
    </div>

    <livewire:signature-input />
    @error('application.signature') <p class="form__input-group__element__error">{{ $message }}</p> @enderror

    <p>
        Note : Vos droits concernant le prélèvement sont expliqués dans un document que vous pouvez obtenir auprès de votre banque.
    </p>
    <p>
        Les informations contenues dans le présent mandat, qui doit être complété, sont destinées à n’être utilisées par le créancier que pour la gestion de sa
        relation avec son client. Elles pourront donner lieu à l’exercice, par ce dernier, de ses droits d’oppositions, d’accès et de rectification tels que prévus aux
        articles 38 et suivants dela loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.
    </p>
    <p>
        Votre référence unique de mandat (RUM) vous sera transmise, avec votre carte d'adhérent-e. Celle-ci vous sera
        envoyée une fois par an ; elle accompagnera le justificatif de paiement de cotisations.
        Pour toute information, vous pouvez nous joindre par mail.
    </p>
</div>
@endif
