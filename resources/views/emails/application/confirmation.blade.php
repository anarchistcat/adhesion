Bonjour, nous avons bien reçu ta demande d'adhésion.<br>
<br>
Nous la traiterons dans les meilleurs délais.<br>
Si ta situation nécessite une aide d'urgence, contactez nous par mail à contact@solidairesinformatique.org<br>
<br>
@if($application->contribution_way === \App\Enums\ContributionWay::CHECK)
Pense à envoyer ta cotisation par chèque à l'adresse suivante :<br>
Solidaires Informatique, 31 rue de la Grange aux Belles 75010 Paris<br>
@elseif($application->contribution_way === \App\Enums\ContributionWay::TRANSFER)
Voici notre RIB pour envoyer ta cotisation :<br>
IBAN : FR76 4255 9100 0008 0038 6183 890<br>
BIC : CCOPFRPPXXX<br>
<br>
Si possible programme un virement automatique mensuel avec comme motif :<br>
Cotisation {{ mb_strtoupper($application->firstname) }} {{ mb_strtoupper($application->name) }}<br>
@endif
<br>
Tu vas recevoir un e-mail lorsque ton adhésion aura été approuvée !
<br>
<br>
Solidaires Informatique<br>
Ne restez pas isolé, syndiquez-vous !<br>
