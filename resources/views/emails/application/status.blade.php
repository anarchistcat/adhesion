{{ __("dashboard.todo-description.{$application->status->value}") }}@if($by !== null) (par {{ $by }})@endif,
<br>
{{ $instruction }}
<br>
<br>
{{ $application->firstname }} {{ mb_substr($application->name, 0, 1) }}. (#{{ $application->getShortenedId() }})
<br>
<br>
À faire :
@livewire('dashboard.to-do')
