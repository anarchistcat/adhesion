Bonjour {{ $application->firstname }}, ton adhésion vient d'être prise en compte.<br>
<br>
@if($application->contribution_way === \App\Enums\ContributionWay::SEPA)
Le premier prélèvement aura lieu le {{ (new DateTime($application->first_withdrawal_date))->format('d/m/y') }}. Voici la référence unique de mandat (RUM) de prélèvement : {{ $application->RUM }}<br>
Sache que tu peux déduire 66% de tes cotisations de tes impôts. Fin mars de l'année suivante, un mail est transmis qui récapitule et justifie des sommes déclarées.<br>
@endif

Tu trouveras ici un feuillet décrivant le fonctionnement du syndicat :<br>
<a href="https://solidairesinformatique.org/wp-content/uploads/2022/04/SI_Accueil4p-202204.pdf">https://solidairesinformatique.org/wp-content/uploads/2022/04/SI_Accueil4p-202204.pdf</a><br>
<br>

Les listes auxquelles tu es abonné·e sont les suivantes :<br>
- diffusion@listes.solidairesinformatique.org C'est la liste de tous les adhérent·e·s du syndicat. Cette liste est modérée : un message envoyé doit d'abord être approuvé pour éviter les spams.<br>

@if($application->mail_local)
Si une section territoriale couvre ton secteur géographique, tu as aussi été abonné·e à sa liste.<br>
@endif
<br>
Sache que tu bénéficies, comme tout·e salarié·e, de 12 jours de congés dits CFESS (Congé Formation Economique Sociale et Syndicale) pour suivre toutes les formations de ce type qu'organise le syndicat et le CEFI (notre centre de formation). Pour en bénéficier, il faut qu'une formation t'intéresse et que tu fasses ta demande de congé au moins 1 mois avant la date à laquelle la formation débute. Tu trouveras les formations sur notre site : solidairesinformatique.org.<br>
<br>
Encore une fois, sois bienvenu·e, n'hésite pas pour quelque question que tu pourrais avoir. Tu peux les poser au bureau (contact@solidairesinformatique.org) ou au conseil syndical, il y aura toujours quelqu'un pour te répondre.<br>
<br>
<br>
Solidaires Informatique<br>
Ne restez pas isolé, syndiquez-vous !<br>
