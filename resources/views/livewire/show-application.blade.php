<div class="overflow-hidden bg-white shadow border-2 mb-6 sm:rounded-lg">
    <div class="border-b mb-2 flex justify-between px-4 py-5 sm:px-6 align-middle">
        <div class="flex">
            <h3 class="m-auto text-lg leading-6 text-gray-900">
                {{ $application->firstname }} {{ $application->name }}
                <span title="{{ $application->id }}" class="font-mono text-xs">{{ $application->getShortenedId() }}</span>
            </h3>
        </div>
        <div class="flex items-baseline flex-wrap">
            <p class="mx-1">
                {{ __("dashboard.status.{$application->status->value}", ['timeDiff' => $application->updated_at->diffForHumans()]) }}
            </p>

            <div class="flex flex-wrap">
                @if($showDone)
                    <form wire:submit="done" class="mx-1">
                        <input class="form__submit w-full my-1 md:w-auto" type="submit" value="{{$doneName}}" />
                    </form>
                @endif
                @if($showReject)
                    {{-- TODO: confirmation with comment --}}
                    <form wire:submit="reject" class="mx-1">
                        <input class="form__submit form__submit_error w-full my-1 md:w-auto" type="submit" value="{{ __('dashboard.actions.reject') }}" />
                    </form>
                @endif
            </div>
        </div>
    </div>

    <livewire:application-overview :application="$application" :showRestrictedInformation="true" />
</div>

