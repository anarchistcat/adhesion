<form wire:submit="submit" class="form">
    @csrf
    <x-honeypot livewire-model="extraFields" />

    @include('application.application-steps.nav')

    @switch($step)
        @case(1)
            @include('application.application-steps.1-personal-data')
            @break
        @case(2)
            @include('application.application-steps.2-company')
            @break
        @case(3)
            @include('application.application-steps.3-contribution')
            @break
        @case(4)
            @include('application.application-steps.4-finalization')
            @break
    @endswitch

    @if($step > 1)
        <button
            wire:click.prevent="back"
            class="form__submit_white"
        >Retour</button>
    @endif
    @if($step === 4)
        <button type="submit" class="form__submit @error('submit') form__submit_error @enderror">
            <span>{{ __('form.send_application') }}</span>
        </button>
    @else
        <input
            wire:click.prevent="next"
            type="submit"
            class="form__submit @error('submit') form__submit_error @enderror"
            value="Suivant"
        >
    @endif
    @error('submit') <p class="form__input-group__element__error">{{ $message }}</p> @enderror

</form>

<script>
    document.addEventListener('livewire:init', () => {
        Livewire.hook('component.init', ({ component }) => {
            if (component.name === 'signature-input') {
                function debounce(func){
                    let timer;
                    return (...args) => {
                        clearTimeout(timer);
                        timer = setTimeout(() => { func.apply(this, args); }, 300);
                    };
                }
                const sendToLivewire = debounce(() => @this.dispatch('updateSignature', [canvas.toDataURL()]));

                const canvas = document.getElementById('signature-freehand');
                canvas.classList.remove('!hidden');

                const mouse = {x: 0, y: 0};
                const last_mouse = {x: 0, y: 0};
                canvas.addEventListener('mousemove', function(e) {
                    last_mouse.x = mouse.x;
                    last_mouse.y = mouse.y;
                    mouse.x = e.pageX - this.offsetLeft;
                    mouse.y = e.pageY - this.offsetTop;
                });

                let ctx = canvas.getContext('2d')
                const onPaint = () => {
                    ctx.beginPath();
                    ctx.moveTo(last_mouse.x, last_mouse.y);
                    ctx.lineTo(mouse.x, mouse.y);
                    ctx.closePath();
                    ctx.stroke();
                    sendToLivewire();
                };

                canvas.addEventListener('mousedown', () => canvas.addEventListener('mousemove', onPaint));
                canvas.addEventListener('mouseup', () => canvas.removeEventListener('mousemove', onPaint));

                {
                    const resetBtn = document.getElementById('reset-signature')
                    resetBtn.classList.remove('hidden');
                    resetBtn.addEventListener('click', () => {
                        ctx.clearRect(0, 0, canvas.width, canvas.height);
                        @this.dispatch('updateSignature');
                    })
                }
            }
        })
    })
</script>
