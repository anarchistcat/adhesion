<div>
    <section>
        <header class="flex flex-wrap">
            <h3 class="text-2xl font-bold mr-2">À faire</h3>
            <p class="self-end">Cliquez sur une case pour effectuer les tâches correspondantes.</p>
        </header>
        @livewire('dashboard.to-do')
    </section>

    @switch($filter)
        @case('new')
            <livewire:show-applications
                :status="['new']"
                title="Voici les nouvelles demandes d'adhésion à valider."
                subtitle="
                Cette étape permet de vérifier que l'entreprise est dans notre champs de syndicalisation.
                Elle permet aussi de vérifier que l'adhésion n'est pas suspecte."
            />
            @break

        @case('validated')
{{--            <p class="text-red-800">Vous n'êtes pas trésorier·e, vous ne pouvez pas completer une adhésion.</p>--}}
            <livewire:show-applications
                :status="['validated']"
                title="Voici les demandes d'adhésion à finaliser par la trésorerie. "
                subtitle="Celles-ci ont été validées par un membre du bureau."
            />
            @break

        @case('completed')
            <livewire:show-applications
                :status="['completed']"
                title="Voici les adhérent·es à inscrire aux listes mails."
                subtitle="Ces adhésions ont été complétés par la trésorerie, c'est l'étape qui déclenche le mail de bienvenue."
            />
            @break

        @case('ready')
            <livewire:show-applications
                :status="['ready']"
                title="Voici les adhérent·es à accueillir."
                subtitle="Iels ont reçu le mail de bienvenue, il faut maintenant organiser une visio d'accueil."
            />
            @break
    @endswitch


</div>
