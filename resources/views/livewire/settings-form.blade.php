<div class="w-full max-w-4xl mx-auto mt-2">
{{--    Page non utilisée --}}
    <header class="mb-2 text-center">
        <h1 class="text-4xl">Paramètres</h1>
    </header>

    @if (session('error'))
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <p class="text-red-500">
                    {{ session('error') }}
                </p>
            </div>
        </div>
    @endif
    @if (session('error'))
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <p class="text-red-500">
                    {{ session('error') }}
                </p>
            </div>
        </div>
    @endif

    <div class="mt-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white px-4 py-5 sm:px-6 rounded">
                <form wire:submit="submit" method="POST" class="form">
                    @csrf

                    <div class="form__input-group">
                        <div class="form__input-group__element">
                            <label class="form__input-group__element__label" for="pseudo">
                                Mail du groupe d'accueil
                            </label>
                            <input
                                wire:model="welcome_group_mail.value"
                                type="email"
                                required
                                class="form__input-group__element__input @error('welcome_group_mail.value') errored @enderror"
                            >
                            @error('welcome_group_mail.value') <p class="form__input-group__element__error">{{ $message }}</p> @enderror
                        </div>
                    </div>

                    <button type="submit" class="form__submit @error('submit') form__submit_error @enderror">
                        <span>Mettre à jour</span>
                    </button>
                    @error('submit')
                    <p class="form__input-group__element__error">{{ $message }}</p>
                    @enderror
                </form>
            </div>
        </div>
    </div>
</div>
