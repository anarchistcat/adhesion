<section>
    <p class="text-xl">
        {{ $title }}
    </p>
    <p>
        {{ $subtitle }}
    </p>

    <div class="max-w-7xl mx-auto sm:p-6 lg:p-8">
        @if($applications->count() === 0)
            <p>{{ __('dashboard.no_applications_found') }}</p>
        @endif

        @foreach ($applications as $application)
            <livewire:show-application :application="$application" wire:key="{{ $application->id }}" />
        @endforeach
    </div>
</section>
