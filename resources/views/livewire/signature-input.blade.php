<div class="form__input-group">
    <div class="form__input-group__element">
        @if($signatureFile === null)
            <label class="form__input-group__element__label" for="signature">
                Dessinez votre signature
            </label>
            <canvas
                id="signature-freehand"
                class="form__input-group__element__input border-black bg-white max-w-md !p-0 !hidden"
                style="height: 100px; width: 300px"
                width="300px"
                height="100px"
            >
                Votre navigateur ne supporte pas la technologie canvas, téléversez un scan de votre signature <label for="signature-file" class="cursor-pointer underline">en cliquant ici</label>.
            </canvas>
            <button id="reset-signature" class="form__submit_white my-2 hidden">Effacer</button>
        @endif

        <label>
            <span class="form__input-group__element__label">En cas d'impossibilité, téléversez votre signature</span> (PNG et JPG, maximum 200 kilo-octets)
            <input type="file" wire:model="signatureFile">
        </label>
    </div>
</div>
