<div class="my-4 px-6">
    <h3 class="font-bold">Bulletin d'adhésion</h3>
    <dl class="grid md:grid-cols-3 grid-cols-2 gap-4 px-6">
        <div>
            <dt class="text-gray-600">{{ __('validation.attributes.name') }}</dt>
            <dd>{{ mb_strtoupper($application->name) }}</dd>
        </div>
        <div>
            <dt class="text-gray-600">{{ __('validation.attributes.firstname') }}</dt>
            <dd>{{ mb_strtoupper($application->firstname) }}</dd>
        </div>
        @if($showRestrictedInformation)
            <div>
                <dt class="text-gray-600">{{ __('validation.attributes.address') }}</dt>
                <dd>{{ $application->address }}</dd>
            </div>
        @endif
        <div>
            <dt class="text-gray-600">{{ __('validation.attributes.postcode') }}</dt>
            <dd>{{ $application->postcode }}</dd>
        </div>
        <div>
            <dt class="text-gray-600">{{ __('validation.attributes.city') }}</dt>
            <dd>{{ $application->city }}</dd>
        </div>
        <div>
            <dt class="text-gray-600">{{ __('validation.attributes.email') }}</dt>
            <dd>{{ $application->email }}</dd>
        </div>
        <div>
            <dt class="text-gray-600">{{ __('validation.attributes.mobile') }}</dt>
            <dd>{{ $application->mobile }}</dd>
        </div>
        @if($application->birthyear)
            <div>
                <dt class="text-gray-600">{{ __('validation.attributes.birthyear') }}</dt>
                <dd>{{ $application->birthyear }}</dd>
            </div>
        @endif
        @if($application->gender)
            <div>
                <dt class="text-gray-600">{{ __('validation.attributes.gender') }}</dt>
                <dd>{{ $application->gender }}</dd>
            </div>
        @endif

        @if($showRestrictedInformation)
            <div>
                <dt class="text-gray-600">{{ __('validation.attributes.contribution') }}</dt>
                <dd>{{ $application->contribution }}</dd>
            </div>

            <div>
                <dt class="text-gray-600">{{ __('validation.attributes.contribution') }}</dt>
                <dd>{{ __("form.contribution_way_value.{$application->contribution_way->value}") }}</dd>
            </div>
        @endif

        <hr class="md:col-span-3 col-span-2">
        @if($application->company_name || $application->company_legal_address || $application->company_work_address)
            <div>
                <dt class="text-gray-600">{{ __('validation.attributes.company_name') }}</dt>
                <dd>{{ $application->company_name }}</dd>
            </div>
            @if($application->company_siret)
                <div>
                    <dt class="text-gray-600">SIRET</dt>
                    <dd>{{ $application->company_siret }}</dd>
                </div>
            @endif
            @if($application->company_legal_address)
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.company_legal_address') }}</dt>
                    <dd>{{ $application->company_legal_address }}</dd>
                </div>
            @endif
            @if($application->company_work_address)
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.company_work_address') }}</dt>
                    <dd>{{ $application->company_work_address }}</dd>
                </div>
            @endif
        @else
            <div class="md:col-span-3 col-span-2">
                <dt class="text-gray-600">{{ __('validation.attributes.company_name') }}</dt>
                <dd>Aucunes informations</dd>
            </div>
        @endif
        <hr class="md:col-span-3 col-span-2">

        @if($application->applicant_comment)
            <div class="col-span-3">
                <dt class="text-gray-600">{{ __('validation.attributes.comment') }}</dt>
                <dd>{{ $application->applicant_comment }}</dd>
            </div>
        @endif

        @if($application->id !== null)
            <div class="md:col-span-3 col-span-2"></div>
            <div>
                <dt class="text-gray-600">Liste mail section territoriale</dt>
                <dd>@if($application->mail_local) Oui @else Non @endif</dd>
            </div>
        @endif
    </dl>

    @if($showRestrictedInformation && $application->contribution_way === \App\Enums\ContributionWay::SEPA)
        <div>
            <h3 class="font-bold mt-8">Mandat SEPA</h3>
            <dl class="grid md:grid-cols-3 grid-cols-2 gap-4 px-6">
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.full_name') }}</dt>
                    <dd>{{ $application->sepa_full_name }}</dd>
                </div>
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.address') }}</dt>
                    <dd>{{ $application->sepa_address }}</dd>
                </div>
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.postcode') }}</dt>
                    <dd>{{ $application->sepa_postcode }}</dd>
                </div>
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.city') }}</dt>
                    <dd>{{ $application->sepa_city }}</dd>
                </div>
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.country') }}</dt>
                    <dd>{{ $application->sepa_country }}</dd>
                </div>
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.iban') }}</dt>
                    <dd>{{ $application->iban }}</dd>
                </div>
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.bic') }}</dt>
                    <dd>{{ $application->bic }}</dd>
                </div>
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.made_at') }}</dt>
                    <dd>{{ $application->made_at }}</dd>
                </div>
                <div>
                    <dt class="text-gray-600">{{ __('form.current_date') }}</dt>
                    <dd>{{ ($application->id === null ? now() : $application->created_at)->format('d/m/Y') }}</dd>
                </div>
                <div>
                    <dt class="text-gray-600">{{ __('validation.attributes.signature') }}</dt>
                    <img src="{{ $application->signature }}" alt="" width="150">
                </div>
            </dl>
        </div>
    @endif
</div>
