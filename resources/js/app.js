import './bootstrap';

import { Livewire } from '../../vendor/livewire/livewire/dist/livewire.esm'
 
import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();

Livewire.start()