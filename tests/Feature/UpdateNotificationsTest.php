<?php

use App\Livewire\Dashboard\ToDo;
use App\Models\Option;
use App\Models\User;
use App\Enums\OptionEnum;
use Livewire\Livewire;

test('no error without changes', function () {
    /** @var User $user */
    $this->actingAs($user = User::factory()->create());

    Option::factory()->create([
        'key' => OptionEnum::COMPLETED_RECAP->value,
        'user_id' => $user->id,
        'value' => '1'
    ]);

    $notifications = Livewire::test(ToDo::class)
        ->assertHasNoErrors()
        ->get('notifications')
    ;

    assert($notifications === ['completed']);

    expect($user->option(OptionEnum::COMPLETED_RECAP)->refresh()->value)->toBe('1');
});

test('notifications can be updated', function () {
    $this->actingAs($user = User::factory()->create());

    Option::factory()->create([
        'key' => OptionEnum::NEW_RECAP->value,
        'user_id' => $user->id,
        'value' => '0'
    ]);
    Option::factory()->create([
        'key' => OptionEnum::COMPLETED_RECAP->value,
        'user_id' => $user->id,
        'value' => '1'
    ]);

    Livewire::test(ToDo::class)
        ->updateProperty('notifications', ['new'])
        ->assertHasNoErrors()
    ;

    expect($user->option(OptionEnum::NEW_RECAP)->refresh()->value)->toBe('1')
        ->and($user->option(OptionEnum::COMPLETED_RECAP)->refresh()->value)->toBe('0');
});


test('new notifications can be updated', function () {
    $this->actingAs($user = User::factory()->create());

    Livewire::test(ToDo::class)
        ->updateProperty('notifications', ['completed'])
        ->assertHasNoErrors()
    ;

    expect($user->option(OptionEnum::COMPLETED_RECAP)->refresh()->value)->toBe('1');
});
