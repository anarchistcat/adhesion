<?php

declare(strict_types=1);

namespace Tests\Feature\Livewire;

use App\Enums\ContributionWay;
use App\Livewire\ApplicationForm;
use App\Mail\ApplicationReceivedConfirmation;
use App\Models\Application;
use Database\Factories\ApplicationFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Livewire\Features\SupportTesting\Testable;
use Livewire\Livewire;
use Tests\TestCase;

class ApplicationFormTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function can_submit_application(): void
    {
        Mail::fake();

        $application = ApplicationFactory::new()->make();
        Livewire::test(ApplicationForm::class, ['application' => $application])
            ->set('wage', 1312)
            ->set('terms', true)
            ->call('submit')
            ->assertHasNoErrors()
            ->assertOk();

        $this->assertTrue(Application::query()
            ->where('name', $application->name)
            ->where('firstname', $application->firstname)
            ->exists());

        Mail::assertSent(ApplicationReceivedConfirmation::class, 1);
    }

    /** @test */
    function cant_go_to_next_step_with_error(): void
    {
        $test = $this->firstStep()
            ->set('application.mobile')
            ->call('next');
        $test->assertHasErrors('application.mobile');
        $this->assertStep1($test);
    }

    /** @test */
    function can_go_to_next_step(): void
    {
        $test = $this->firstStep()
            ->call('next')
            ->assertHasNoErrors()
            ->assertOk();
        $this->assertStep2($test);
    }

    /** @test */
    function can_go_back_in_application(): void
    {
        $test = $this->firstStep()->call('next')->call('back');
        $this->assertStep1($test);
    }

    /** @test */
    function can_fill_sepa(): void
    {
        Mail::fake();
        $test = $this->firstStep()->set('step', 3);
        $test->assertSee('Mandat de prélèvement SEPA');

        $test->set('wage', '2000');
        $test->assertSet('application.contribution', 13);

        $test->set('application.sepa_full_name', fake()->name());
        $test->set('application.sepa_address', fake()->address());
        $test->set('application.sepa_postcode', fake()->randomNumber());
        $test->set('application.sepa_city', fake()->city());
        $test->set('application.sepa_country', 'France');
        $test->set('application.iban', 'GB82 WEST 1234 5698 7654 32');
        $bic = fake()->swiftBicNumber();
        $test->set('application.bic', $bic);
        $test->set('application.made_at', fake()->city());
        $test->set('application.signature', 'data:image/png;base64,fakedatatofillthebase64SwAAAABkCAMAAAAL3/3=');

        $test->assertHasNoErrors()->assertOk();
        $test->call('next');
        $test->assertHasNoErrors()->assertOk();

        $test->assertSet('step', 4);
        $test->assertSee('Récapitulatif de vos informations');
        $test->assertSee($bic);

        $test->set('terms', true)
            ->call('submit')
            ->assertHasNoErrors()
            ->assertOk();

        Mail::assertSent(ApplicationReceivedConfirmation::class, 1);

        $this->assertTrue(Application::query()
            ->where('bic', $bic)
            ->where('contribution', 13)
            ->exists());
    }

    /** @test */
    function cant_fill_sepa_data_on_others_contribution_way(): void
    {
        Mail::fake();

        $test = $this->firstStep()->set('step', 3);
        $test->assertSee('Mandat de prélèvement SEPA');

        $test->set('application.contribution_way', ContributionWay::TRANSFER);
        $test->assertDontSee('Mandat de prélèvement SEPA');

        $test->set('application.contribution_way', ContributionWay::CHECK);
        $test->assertDontSee('Mandat de prélèvement SEPA');

        $test->set('application.contribution_way', ContributionWay::SEPA);
        $test->assertSee('Mandat de prélèvement SEPA');

        $test->set('application.contribution_way', ContributionWay::TRANSFER);
        $test->assertDontSee('Mandat de prélèvement SEPA');

        $test->set('minimalContribution', true);
        $test->assertSet('application.contribution', 1);

        // To verify if after filling information and switching to another contribution way it's not saved.
        $bic = fake()->swiftBicNumber();
        $test->set('application.bic', $bic);

        $test->assertHasNoErrors()->assertOk();
        $test->call('next');
        $test->assertHasNoErrors()->assertOk();

        $test->assertSet('step', 4);
        $test->assertSee('Récapitulatif de vos informations');
        $test->assertDontSee('Mandat SEPA');
        $test->assertDontSee($bic);

        $test->set('terms', true)
            ->call('submit')
            ->assertHasNoErrors()
            ->assertOk();

        Mail::assertSent(ApplicationReceivedConfirmation::class, 1);

        $this->assertFalse(Application::query()
            ->where('bic', $bic)
            ->exists());

        $this->assertTrue(Application::query()
            ->where('contribution_way', ContributionWay::TRANSFER->value)
            ->where('contribution', 1)
            ->exists());
    }

    private function firstStep(): Testable
    {
        return Livewire::test(ApplicationForm::class, ['application' => new Application()])
            ->set('application.name', fake()->name())
            ->set('application.firstname', fake()->name())
            ->set('application.address', fake()->address())
            ->set('application.postcode', fake()->randomNumber())
            ->set('application.city', fake()->city())
            ->set('application.email', fake()->safeEmail())
            ->set('application.mobile', fake()->phoneNumber());
    }

    private function assertStep1(Testable $test): void
    {
        $test->assertSet('step', 1);
        $test->assertSee('Année de naissance');
        $test->assertDontSee("J'ai un employeur");
    }

    private function assertStep2(Testable $test): void
    {
        $test->assertSet('step', 2);
        $test->assertDontSee('Année de naissance');
        $test->assertSee("J'ai un employeur");
    }
}
