<?php

namespace Database\Seeders;

use App\Enums\ApplicationStatus;
use App\Enums\ContributionWay;
use App\Models\Application;
use App\Services\FileService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Storage::fake('nextcloud');
        Application::factory()->create([
            'status' => ApplicationStatus::REJECTED
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::NEW,
            'contribution_way' => ContributionWay::SEPA,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::NEW,
            'contribution_way' => ContributionWay::CHECK,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::VALIDATED,
            'contribution_way' => ContributionWay::SEPA,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::VALIDATED,
            'contribution_way' => ContributionWay::TRANSFER,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::VALIDATED,
            'contribution_way' => ContributionWay::CHECK,
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::COMPLETED
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::READY
        ]);

        Application::factory()->create([
            'status' => ApplicationStatus::WELCOMED
        ]);
    }
}
