<?php

namespace Database\Seeders;

use App\Enums\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        (new UserSeeder())->run();

        /** To have the ability to seed applications with all status, we log as an admin */
        Auth::login(User::query()->where('role', Role::ADMIN)->first());
        (new ApplicationSeeder())->run();
        Auth::logout();
    }
}
