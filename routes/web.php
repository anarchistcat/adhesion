<?php

declare(strict_types=1);

use App\Enums\Role;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\PasswordResetLinkController;
use App\Livewire\UserForm;
use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Features;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', static fn () => view('application.create'));
Route::get('/bulletin-adhesion-pdf', static fn () => redirect('/BulletinAdhesion.pdf'));
Route::get('/success', static fn () => view('application.success'))->name('success');

Route::get('/archives', static fn () => view('application.archives'))
    ->name('archives')
    ->middleware(['auth', 'role:'.Role::BOARD->value]);

Route::prefix('admin')
    ->middleware(['auth', 'role:'.Role::ADMIN->value, config('jetstream.auth_session'), 'verified'])
    ->group(static function (): void {
        Route::get('', static fn () => view('admin.index', ['ref' => base_path().'/.git/refs/heads/main']))->name('admin.index');

        Route::prefix('logs')->group(static function (): void {
            Route::get('', [AdminController::class, 'viewLogs'])->name('admin.logs');
            Route::get('clear', [AdminController::class, 'clearLogs'])->name('admin.logs.clear');
        });

        Route::prefix('users')->group(static function (): void {
            Route::get('', [AdminController::class, 'viewUsers'])->name(AdminController::ROUTE_NAMES['users']['index']);
            Route::get('/edit/{user}', UserForm::class)->name(AdminController::ROUTE_NAMES['users']['edit']);
            Route::get('/create', UserForm::class)->name(AdminController::ROUTE_NAMES['users']['create']);
        });

//        Route::get('/settings', SettingsForm::class)->name('admin.settings');
    });

Route::prefix('dashboard')
    ->middleware(['auth', 'role:'.Role::BOARD->value, config('jetstream.auth_session'), 'verified'])
    ->group(function (): void {
        Route::get('', \App\Livewire\Dashboard\Dashboard::class)
            ->name('dashboard.index')
            ->middleware(['auth', 'role:'.Role::BOARD->value]);

        Route::prefix('application')->group(function (): void {
            Route::get(
                'complete/{application}',
                \App\Livewire\ApplicationComplete::class
            )->name('dashboard.application.complete')
                ->middleware(['auth', 'role:'.Role::TREASURY->value]);
        });
});

if (Features::enabled(Features::resetPasswords())) {
    // Overwrite the forgot-password post route to add enumeration protections
    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware(['guest:'.config('fortify.guard')])
        ->name('password.email');
}
