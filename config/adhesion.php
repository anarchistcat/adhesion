<?php

use SolidairesInformatique\AccessToken;

return [
    'sirene' => [
        'key' => env('SIRENE_API_KEY'),
        'secret' => env('SIRENE_API_SECRET'),
    ],
];
