# adhesion

Plateforme d'adhésion de [Solidaires Informatique](https://solidairesinformatique.org), en ligne à [adhesion.solidairesinformatique.org](https://adhesion.solidairesinformatique.org/).

## Développement

Pour simplifier les commandes, on considère qu'un alias `sail` existe dans votre configuration.

```bash
alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'
```

Si ce n'est pas le cas, remplacez `sail` par `vendor/bin/sail`.

### Installation pour le développement

Prérequis :

-   php
-   docker

```bash
cp .env.example .env
sail up
sail artisan key:generate
# Installation des dépendances
sail composer install
sail npm install
# Initialisation de la base de données
sail artisan migrate:fresh --seed
# Compilation des assets
sail npm run build
```

Le projet est disponible à [http://localhost/](http://localhost/)
L'interface pour voir les mails envoyés est sur [http://localhost:8025/](http://localhost:8025/)

Pour arrêter les containers :

```bash
sail stop # ou sail down pour les arrêter et les supprimer
```

Une fois installé, pour lancer le projet il suffit d'exécuter

```bash
sail up
sail npm run dev
```

### Outils

Vérification des règles de style :

```bash
vendor/bin/php-cs-fixer fix app/ --dry-run -v --diff
```

Analyse statique pour détecter d'éventuels bugs :

```bash
vendor/bin/phpstan analyse --memory-limit=500M
```

Tests PHP :

```bash
sail test --parallel
```

### API SIRENE

Pour la récupération des informations de l'entreprise avec le numéro siret :

1. Créer un compte sur https://api.insee.fr/catalogue/.
2. Ajouter SIRENE à l'application créée par défaut dans votre compte.
3. Remplir `SIRENE_API_KEY` et `SIRENE_API_SECRET` dans le fichier `.env`.
4. `sail restart`

## Déploiement

Le serveur Web doit diriger toutes les requêtes vers `public/index.php`.

Besoins:

-   PHP >= 8.3
-   BCMath PHP Extension
-   Ctype PHP Extension
-   cURL PHP Extension
-   DOM PHP Extension
-   Fileinfo PHP Extension
-   JSON PHP Extension
-   Mbstring PHP Extension
-   OpenSSL PHP Extension
-   PCRE PHP Extension
-   PDO PHP Extension
-   Tokenizer PHP Extension
-   XML PHP Extension

Pour que les tâches programmées soit exécutées la cron suivante est nécessaire :

```shell
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

Le site est déployé automatiquement avec [ansible](./ansible/readme.md).
